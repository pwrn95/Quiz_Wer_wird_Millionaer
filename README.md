Wer wird Millionaer?
Es handelt sich um ein Quizgame mit 15 Fragen mit 4 Antwortmöglichkeiten
und jeweils einer richtigen Lösung. Bei der richtigen Antowrt folgt automatisch
die nächste Frage, bei einer flaschen Antwort wird das Spiel beendet. Außerdem
hat man 3 (oder 4) Joker, die man jederzeit einsetzen kann (50/50, Publikum und
Telefon). Bei richtiger Antwort auf die 15. Frage ist das Spiel abgeschlossen
und man hat gewonnen.

Die grafische Ausgabe erfolgt mit BOS. Dort wird angezeigt: Die Frage, die 4
anklickbaren Antwortmöglichkeiten, der Spielfortschritt, die verlbeibenden Joker,
Feedback über richtige/flasche Antwort, Mitteilung über Gewinn bei beenden des 
Spiels.

Die größte Herausforderung ist eine realistische Umsetzung der Joker, vorallem
des Publikums- und Telefonjokers.

Löungsansatz: 

Publikumsjoker: Für die 4 Antwortmöglichkeiten werden Prozentwerte zufällig ermittelt, die
zusammen 100% ergeben, wobei die richtige Antwort mit erhöhter Wahrscheinlichkeit
den höchsten Prozentwert erhält.

Telefonjoker: Der Joker gibt zu 70% die richtige Antwort, zu 20% die Falsche 
und mit einer 10%igen Wahrscheinlichkeit weiß er keine Lösung. Diese Werte
werden zufällig ermittelt.
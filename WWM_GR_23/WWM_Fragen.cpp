

QuestionA a0, a1, a2, a3, a4;

// Frage a0
strcpy_s(a0.q, "Peter Kreuder komponierte 1936 den Schlager 'Ich wollt' ich w�r' ein ...'?");
strcpy_s(a0.a, "Hund");
strcpy_s(a0.b, "Huhn");
strcpy_s(a0.c, "Hummer");
strcpy_s(a0.d, "Huflattich");
strcpy_s(a0.ans, "B");

// Frage a1
strcpy_s(a1.q, "Wie nennt man gro�en Wirbel um eine ber�hmte Pers�nlichkeit?");
strcpy_s(a1.a, "Prominentenkirmes");
strcpy_s(a1.b, "Starrummel");
strcpy_s(a1.c, "VIP-Jahrmarkt");
strcpy_s(a1.d, "G�tterzirkus");
strcpy_s(a1.ans, "B");

// Frage a2
strcpy_s(a2.q, "Was kann man essen?");
strcpy_s(a2.a, "Aktenkoffer");
strcpy_s(a2.b, "Schulranzen");
strcpy_s(a2.c, "Rucksack");
strcpy_s(a2.d, "Apfeltasche");
strcpy_s(a2.ans, "D");

// Frage a3
strcpy_s(a3.q, "Im Gegensatz zur Spinne ist die Fliege ein ...?");
strcpy_s(a3.a, "Inbier");
strcpy_s(a3.b, "Inschnaps");
strcpy_s(a3.c, "Inwein");
strcpy_s(a3.d, "Insekt");
strcpy_s(a3.ans, "D");

// Frage a4
strcpy_s(a4.q, "Wobei handelt es sich nicht um ein Reptil?");
strcpy_s(a4.a, "Wiesenotter");
strcpy_s(a4.b, "Kreuzotter");
strcpy_s(a4.c, "Puffotter");
strcpy_s(a4.d, "Eidotter");
strcpy_s(a4.ans, "D");

QuestionB b0, b1, b2, b3, b4;

// Frage b0
strcpy_s(b0.q, "Wobei handelt es sich um ein Notsignal im internationalen Funkverkehr?");
strcpy_s(b0.a, "Mayday");
strcpy_s(b0.b, "Down Town");
strcpy_s(b0.c, "Jetset");
strcpy_s(b0.d, "Flower Power");
strcpy_s(b0.ans, "A");

// Frage b1
strcpy_s(b1.q, "Was steht �ber dem Haupteingang des Reichstagsgeb�udes in Berlin?");
strcpy_s(b1.a, "Dem deutschen Volke");
strcpy_s(b1.b, "Achtung, Bundestag!");
strcpy_s(b1.c, "Nur f�r Abgeordnete");
strcpy_s(b1.d, "Bitte nicht st�ren!");
strcpy_s(b1.ans, "A");

// Frage b2
strcpy_s(b2.q, "Kleine S�nden straft der liebe Gott dem Volksmund nach ...?");
strcpy_s(b2.a, "sofort");
strcpy_s(b2.b, "so gut wie nie");
strcpy_s(b2.c, "mit Fernsehverbot");
strcpy_s(b2.d, "in neun Monaten");
strcpy_s(b2.ans, "A");

// Frage b3
strcpy_s(b3.q, "Welches Imitat erf�llt sichtlich seinen Zweck?");
strcpy_s(b3.a, "Tomatenr�ter");
strcpy_s(b3.b, "Kaffeewei�er");
strcpy_s(b3.c, "Senfgelber");
strcpy_s(b3.d, "Schokoladenbr�uner");
strcpy_s(b3.ans, "B");

// Frage b4
strcpy_s(b4.q, "F�r Touristen aus aller Welt liegt Bad Kreuznach auf jeden Fall ...?");
strcpy_s(b4.a, "ganz nah");
strcpy_s(b4.b, "in der N�he");
strcpy_s(b4.c, "an der Nahe");
strcpy_s(b4.d, "in weiter Ferne so nah");
strcpy_s(b4.ans, "C");

QuestionC c0, c1, c2, c3, c4;

// Frage c0
strcpy_s(c0.q, "Wie hei�t - laut einem M�rchen der Br�der Grimm - die Schwester von Schneewei�chen?");
strcpy_s(c0.a, "Fliederlila");
strcpy_s(c0.b, "Maisgelb");
strcpy_s(c0.c, "Rosenrot");
strcpy_s(c0.d, "Kornblumenblau");
strcpy_s(c0.ans, "C");

// Frage c1
strcpy_s(c1.q, "Wer davonl�uft, der gibt einer Redewendung nach ...?");
strcpy_s(c1.a, "Wechselgeld");
strcpy_s(c1.b, "Fersengeld");
strcpy_s(c1.c, "Kleingeld");
strcpy_s(c1.d, "Kindergeld");
strcpy_s(c1.ans, "B");

// Frage c2
strcpy_s(c2.q, "Eine popul�re Spielart der amerikanischen Volksmusik hei�t Country & ...?");
strcpy_s(c2.a, "Thriller");
strcpy_s(c2.b, "Comedy");
strcpy_s(c2.c, "Slapstick");
strcpy_s(c2.d, "Western");
strcpy_s(c2.ans, "D");

// Frage c3
strcpy_s(c3.q, "Welchen Ausdruck pr�gte der Philosoph Hegel?");
strcpy_s(c3.a, "f�r sich und an");
strcpy_s(c3.b, "und f�r an sich");
strcpy_s(c3.c, "an und f�r sich");
strcpy_s(c3.d, "sich und f�r an");
strcpy_s(c3.ans, "C");

// Frage c4
strcpy_s(c4.q, "Wie nennt man die direkte Verbindung von Plus- und Minuspol einer Spannungsquelle?");
strcpy_s(c4.a, "Glasbruch");
strcpy_s(c4.b, "Wasserschaden");
strcpy_s(c4.c, "Rohrverstopfung");
strcpy_s(c4.d, "Kurzschluss");
strcpy_s(c4.ans, "D");

QuestionD d0, d1, d2, d3, d4;

// Frage d0
strcpy_s(d0.q, "Wer hat von Berufs wegen mit dem Spritzenhaus zu tun?");
strcpy_s(d0.a, "G�rtner");
strcpy_s(d0.b, "Krankenschwester");
strcpy_s(d0.c, "Feuerwehrmann");
strcpy_s(d0.d, "Konditor");
strcpy_s(d0.ans, "C");

// Frage d1
strcpy_s(d1.q, "Durch welches Verfahren schickte man im alten Athen seine Mitb�rger in die Verbannung?");
strcpy_s(d1.a, "G�tterspeise");
strcpy_s(d1.b, "Henkersmahlzeit");
strcpy_s(d1.c, "Scherbengericht");
strcpy_s(d1.d, "Grillteller");
strcpy_s(d1.ans, "C");

// Frage d2
strcpy_s(d2.q, "Vor welchen gesetzlosen Gesellen f�rchteten sich schon die alten Rittersleut'?");
strcpy_s(d2.a, "Geb�schganoven");
strcpy_s(d2.b, "Heckenstrolche");
strcpy_s(d2.c, "Baumbanditen");
strcpy_s(d2.d, "Strauchdiebe");
strcpy_s(d2.ans, "D");

// Frage d3
strcpy_s(d3.q, "Wo befindet sich das Orchester w�hrend einer Opernauff�hrung?");
strcpy_s(d3.a, "Fahrradweg");
strcpy_s(d3.b, "Gr�nstreifen");
strcpy_s(d3.c, "B�schung");
strcpy_s(d3.d, "Graben");
strcpy_s(d3.ans, "D");

// Frage d4
strcpy_s(d4.q, "Nicht nur Ross und Reiter brauchen zum Sieg oft ein gutes ...?");
strcpy_s(d4.a, "Shwedish");
strcpy_s(d4.b, "Denish");
strcpy_s(d4.c, "Finish");
strcpy_s(d4.d, "Norvegish");
strcpy_s(d4.ans, "C");

QuestionE e0, e1, e2, e3, e4;

// Frage e0
strcpy_s(e0.q, "Was ist die Amtssprache von Mexiko?");
strcpy_s(e0.a, "Portugiesisch");
strcpy_s(e0.b, "Englisch");
strcpy_s(e0.c, "Franz�sisch");
strcpy_s(e0.d, "Spanisch");
strcpy_s(e0.ans, "D");

// Frage e1
strcpy_s(e1.q, "Wie nennt man ein gedrucktes Rundschreiben des Papstes?");
strcpy_s(e1.a, "Enzephalitis");
strcpy_s(e1.b, "Enzym");
strcpy_s(e1.c, "Enzyklop�die");
strcpy_s(e1.d, "Enzyklika");
strcpy_s(e1.ans, "D");

// Frage e2
strcpy_s(e2.q, "Was wird nicht durch Reflexe ausgel�st?");
strcpy_s(e2.a, "G�hnen");
strcpy_s(e2.b, "Niesen");
strcpy_s(e2.c, "Kauen");
strcpy_s(e2.d, "Schlucken");
strcpy_s(e2.ans, "C");

// Frage e3
strcpy_s(e3.q, "Welches Wort bedeutet so viel wie 'allgemein verbreitet' oder '�blich'?");
strcpy_s(e3.a, "bodenst�ndig");
strcpy_s(e3.b, "gel�ndeg�ngig");
strcpy_s(e3.c, "landl�ufig");
strcpy_s(e3.d, "stadtfl�chtig");
strcpy_s(e3.ans, "C");

// Frage e4
strcpy_s(e4.q, "F�r Kosmetika und als Nahrungserg�nzungsmittel verwendet man das �l der ...?");
strcpy_s(e4.a, "Wunderkerze");
strcpy_s(e4.b, "Nachtkerze");
strcpy_s(e4.c, "Taufkerze");
strcpy_s(e4.d, "Z�ndkerze");
strcpy_s(e4.ans, "B");

QuestionF f0, f1, f2, f3, f4;

// Frage f0
strcpy_s(f0.q, "Unter welchem Namen sangen Wigald Boning und Olli Dittrich 'Lieder, die die Welt nicht braucht'?");
strcpy_s(f0.a, "Die Beh�mmerten");
strcpy_s(f0.b, "Die Abgedrehten");
strcpy_s(f0.c, "Die Doofen");
strcpy_s(f0.d, "Die Irren");
strcpy_s(f0.ans, "C");

// Frage f1
strcpy_s(f1.q, "Wer sang sich 1974 mit 'Hoch auf dem gelben Wagen' in die Charts?");
strcpy_s(f1.a, "Genscher");
strcpy_s(f1.b, "Scheel");
strcpy_s(f1.c, "Brandt");
strcpy_s(f1.d, "Kinkel");
strcpy_s(f1.ans, "B");

// Frage f2
strcpy_s(f2.q, "Mit den Worten 'Der Weltraum, unendliche Weiten' beginnen die Abenteuer des Raumschiffes ...?");
strcpy_s(f2.a, "Galactica");
strcpy_s(f2.b, "Orion");
strcpy_s(f2.c, "Nostromo");
strcpy_s(f2.d, "Enterprise");
strcpy_s(f2.ans, "D");

// Frage f3
strcpy_s(f3.q, "Der potenzielle Bundespr�sident (2004) Horst K�hler war davor Chef des ...?");
strcpy_s(f3.a, "IWF");
strcpy_s(f3.b, "DRK");
strcpy_s(f3.c, "IOC");
strcpy_s(f3.d, "DFB");
strcpy_s(f3.ans, "A");

// Frage f4
strcpy_s(f4.q, "Welches Ministerium wird umgangssprachlich als Hardth�he bezeichnet?");
strcpy_s(f4.a, "Justiz");
strcpy_s(f4.b, "Finanzen");
strcpy_s(f4.c, "Verteidigung");
strcpy_s(f4.d, "Gesundheit");
strcpy_s(f4.ans, "C");

QuestionG g0, g1, g2, g3, g4;

// Frage g0
strcpy_s(g0.q, "Was ist ein Rebus?");
strcpy_s(g0.a, "Kreditinstitut");
strcpy_s(g0.b, "Werkzeug");
strcpy_s(g0.c, "Weinstock");
strcpy_s(g0.d, "Bilderr�tsel");
strcpy_s(g0.ans, "D");

// Frage g1
strcpy_s(g1.q, "Wie hei�t der K�nig der Erdm�nnchen in der Augsburger Puppenkiste?");
strcpy_s(g1.a, "Kalle Wirsch");
strcpy_s(g1.b, "Klein Zack");
strcpy_s(g1.c, "Zwerg Nase");
strcpy_s(g1.d, "Frodo Beutlin");
strcpy_s(g1.ans, "A");

// Frage g2
strcpy_s(g2.q, "Wer geh�rt nicht zu Dorothys Begleitern in 'Der Zauberer von Oz'?");
strcpy_s(g2.a, "fast kopfloser Nick");
strcpy_s(g2.b, "Vogelscheuche");
strcpy_s(g2.c, "Zinnmann");
strcpy_s(g2.d, "�ngstlicher L�we");
strcpy_s(g2.ans, "A");

// Frage g3
strcpy_s(g3.q, "Womit bescherte Songwriter Leonard Cohen der Welt einen Klassiker?");
strcpy_s(g3.a, "Suzanne");
strcpy_s(g3.b, "Michelle");
strcpy_s(g3.c, "Rosanna");
strcpy_s(g3.d, "Lucille");
strcpy_s(g3.ans, "A");

// Frage g4
strcpy_s(g4.q, "Was kann man zum L�sen festgerosteter Schrauben verwenden?");
strcpy_s(g4.a, "Kriech�l");
strcpy_s(g4.b, "Schleichkerosin");
strcpy_s(g4.c, "Tr�delbenzin");
strcpy_s(g4.d, "Bummeldiesel");
strcpy_s(g4.ans, "A");

QuestionH h0, h1, h2, h3, h4;

// Frage h0
strcpy_s(h0.q, "Wie nennt man von Gletschern transportierten Gesteinsschutt?");
strcpy_s(h0.a, "Mor�ne");
strcpy_s(h0.b, "Mur�ne");
strcpy_s(h0.c, "Morelle");
strcpy_s(h0.d, "Murnau");
strcpy_s(h0.ans, "A");

// Frage h1
strcpy_s(h1.q, "Wer schoss in 62 Fu�ball-L�nderspielen 68 Tore?");
strcpy_s(h1.a, "Gerd M�ller");
strcpy_s(h1.b, "Horst Hrubesch");
strcpy_s(h1.c, "Helmut Rahn");
strcpy_s(h1.d, "Jupp Heynckes");
strcpy_s(h1.ans, "A");

// Frage h2
strcpy_s(h2.q, "Wie hei�t ein Kanton der Schweiz?");
strcpy_s(h2.a, "Baden-Baden");
strcpy_s(h2.b, "Karlsruhe");
strcpy_s(h2.c, "Konstanz");
strcpy_s(h2.d, "Freiburg");
strcpy_s(h2.ans, "D");

// Frage h3
strcpy_s(h3.q, "Eine Shakespeare-Kom�die tr�gt den Titel 'Wie es euch ...'?");
strcpy_s(h3.a, "gef�llt");
strcpy_s(h3.b, "beliebt");
strcpy_s(h3.c, "genehm ist");
strcpy_s(h3.d, "in den Kram passt");
strcpy_s(h3.ans, "A");

// Frage h4
strcpy_s(h4.q, "Ein Stummfilmklassiker von Sergej Eisenstein hei�t 'Panzerkreuzer ...'?");
strcpy_s(h4.a, "Rasputin");
strcpy_s(h4.b, "Potemkin");
strcpy_s(h4.c, "Iljuschin");
strcpy_s(h4.d, "Putin");
strcpy_s(h4.ans, "B");


QuestionI i0, i1, i2, i3, i4;

// Frage i0
strcpy_s(i0.q, "Welches deutsche F�rstenhaus organisierte bis ins 19. Jahrhundert die kaiserliche Reichspost?");
strcpy_s(i0.a, "F�rstenberg");
strcpy_s(i0.b, "Schaumburg-Lippe");
strcpy_s(i0.c, "Hohenlohe");
strcpy_s(i0.d, "Thurn und Taxis");
strcpy_s(i0.ans, "D");

// Frage i1
strcpy_s(i1.q, "Was ist das so genannte Horn von Afrika?");
strcpy_s(i1.a, "Sternbild");
strcpy_s(i1.b, "Halbinsel");
strcpy_s(i1.c, "fossiles Kultobjekt");
strcpy_s(i1.d, "Berg in Tansania");
strcpy_s(i1.ans, "B");

// Frage i2
strcpy_s(i2.q, "Vor allem in Norddeutschland schw�ren traditionsbewusste Genie�er auf 'Birnen, ...'?");
strcpy_s(i2.a, "Hering und Ei");
strcpy_s(i2.b, "Bohnen und Speck");
strcpy_s(i2.c, "Gr�nkohl und Senf");
strcpy_s(i2.d, "Kl��e und Quark");
strcpy_s(i2.ans, "B");

// Frage i3
strcpy_s(i3.q, "Bei den Olympischen Spielen 1900 gab es einen Wettbewerb im Schie�en auf lebende ...?");
strcpy_s(i3.a, "Hasen");
strcpy_s(i3.b, "Ratten");
strcpy_s(i3.c, "Fasane");
strcpy_s(i3.d, "Tauben");
strcpy_s(i3.ans, "D");

// Frage i4
strcpy_s(i4.q, "Wenn �ber die Sangesversuche von K-Fed berichtet wird, geht es um den Ehemann von ...?");
strcpy_s(i4.a, "Jennifer Lopez");
strcpy_s(i4.b, "Beyonc� Knowles");
strcpy_s(i4.c, "Christina Aguilera");
strcpy_s(i4.d, "Britney Spears");
strcpy_s(i4.ans, "D");

QuestionJ j0, j1, j2, j3, j4;

// Frage j0
strcpy_s(j0.q, "Wobei handelt es sich nicht um ein Insekt?");
strcpy_s(j0.a, "Stechm�cke");
strcpy_s(j0.b, "Winterm�cke");
strcpy_s(j0.c, "Grasm�cke");
strcpy_s(j0.d, "Kriebelm�cke");
strcpy_s(j0.ans, "C");

// Frage j1
strcpy_s(j1.q, "In welchem Hitchcock-Film kann die Protagonistin es nicht ertragen, die Farbe Rot zu sehen?");
strcpy_s(j1.a, "Marnie");
strcpy_s(j1.b, "Vertigo");
strcpy_s(j1.c, "Der unsichtbare Dritte");
strcpy_s(j1.d, "Die V�gel");
strcpy_s(j1.ans, "A");

// Frage j2
strcpy_s(j2.q, "Wie hei�t eine Klasse der Wirbeltiere mit mehr als 20.000 bekannten Arten?");
strcpy_s(j2.a, "Schuppenfische");
strcpy_s(j2.b, "Kiemenfische");
strcpy_s(j2.c, "Knochenfische");
strcpy_s(j2.d, "Flossenfische");
strcpy_s(j2.ans, "C");

// Frage j3
strcpy_s(j3.q, "Wohin muss man reisen, wenn man die �berreste der antiken Metropole Pergamon sehen will?");
strcpy_s(j3.a, "T�rkei");
strcpy_s(j3.b, "�gypten");
strcpy_s(j3.c, "Zypern");
strcpy_s(j3.d, "Kreta");
strcpy_s(j3.ans, "A");

// Frage j4
strcpy_s(j4.q, "Wo machte man Mitte des 20. Jh. einen f�r das Bibelverst�ndnis spektakul�ren Schriftrollen-Fund?");
strcpy_s(j4.a, "Qumran");
strcpy_s(j4.b, "Chich�n Itz�");
strcpy_s(j4.c, "Angkor Vat");
strcpy_s(j4.d, "Herculaneum");
strcpy_s(j4.ans, "A");

QuestionK k0, k1, k2, k3, k4;

// Frage k0
strcpy_s(k0.q, "Wie hei�t das traditionelle Pferderennen in Siena?");
strcpy_s(k0.a, "Calcio");
strcpy_s(k0.b, "Palio");
strcpy_s(k0.c, "Barolo");
strcpy_s(k0.d, "Boccia");
strcpy_s(k0.ans, "B");

// Frage k1
strcpy_s(k1.q, "Nach einem Stadtteil welcher Metropole benannte sich der Regisseur Rosa von Praunheim?");
strcpy_s(k1.a, "Magdeburg");
strcpy_s(k1.b, "Frankfurt am Main");
strcpy_s(k1.c, "Hamburg");
strcpy_s(k1.d, "K�ln");
strcpy_s(k1.ans, "B");

// Frage k2
strcpy_s(k2.q, "Wer war nie Kaiser des Heiligen R�mischen Reiches");
strcpy_s(k2.a, "Ludwig der Bayer");
strcpy_s(k2.b, "Heinrich der L�we");
strcpy_s(k2.c, "Otto der Gro�e");
strcpy_s(k2.d, "Friedrich Barbarossa");
strcpy_s(k2.ans, "B");

// Frage k3
strcpy_s(k3.q, "Wie nennen Mathematiker einen Grundsatz, der nicht von anderen S�tzen abgeleitet werden kann?");
strcpy_s(k3.a, "Idiom");
strcpy_s(k3.b, "Axiom");
strcpy_s(k3.c, "Algorithmus");
strcpy_s(k3.d, "Hypothese");
strcpy_s(k3.ans, "B");

// Frage k4
strcpy_s(k4.q, "Welcher dieser vier Vornamen von Mozart wurde ins Lateinische �bertragen zu 'Amadeus'?");
strcpy_s(k4.a, "Joannes");
strcpy_s(k4.b, "Chrysostomus");
strcpy_s(k4.c, "Wolfgangus");
strcpy_s(k4.d, "Theophilus");
strcpy_s(k4.ans, "D");

QuestionL l0, l1, l2, l3, l4;

// Frage l0
strcpy_s(l0.q, "Was ist nach Mah�n, dem Hauptort der Insel Menorca, benannt?");
strcpy_s(l0.a, "Marone");
strcpy_s(l0.b, "Mahagoni");
strcpy_s(l0.c, "Marihuana");
strcpy_s(l0.d, "Mayonnaise");
strcpy_s(l0.ans, "D");

// Frage l1
strcpy_s(l1.q, "Was ist kein Lamellenpilz?");
strcpy_s(l1.a, "Hallimasch");
strcpy_s(l1.b, "Champignon");
strcpy_s(l1.c, "Steinpilz");
strcpy_s(l1.d, "Knollenbl�tterpilz");
strcpy_s(l1.ans, "C");

// Frage l2
strcpy_s(l2.q, "Tabak z�hlt zur selben Pflanzenfamilie wie die ...?");
strcpy_s(l2.a, "Kartoffel");
strcpy_s(l2.b, "Karotte");
strcpy_s(l2.c, "Kamille");
strcpy_s(l2.d, "Kastanie");
strcpy_s(l2.ans, "A");

// Frage l3
strcpy_s(l3.q, "Wessen Romane w�ren nie ver�ffentlicht worden, wenn man seinem letzten Willen entsprochen h�tte?");
strcpy_s(l3.a, "Hermann Hesse");
strcpy_s(l3.b, "Theodor Fontane");
strcpy_s(l3.c, "Franz Kafka");
strcpy_s(l3.d, "Robert Musil");
strcpy_s(l3.ans, "C");

// Frage l4
strcpy_s(l4.q, "Welcher Schriftsteller verfasste 'Die New York - Trilogie'?");
strcpy_s(l4.a, "John Irving");
strcpy_s(l4.b, "Paul Auster");
strcpy_s(l4.c, "Dan Brown");
strcpy_s(l4.d, "Ernest Hemmingway");
strcpy_s(l4.ans, "B");

QuestionM m0, m1, m2, m3, m4;

// Frage m0
strcpy_s(m0.q, "Welcher deutsche Boxer schlug im Juni 1952 den Ringrichter Max Pippow zu Boden?");
strcpy_s(m0.a, "Peter M�ller");
strcpy_s(m0.b, "Eckhard Dagge");
strcpy_s(m0.c, "Bubi Scholz");
strcpy_s(m0.d, "Max Schmeling");
strcpy_s(m0.ans, "A");

// Frage m1
strcpy_s(m1.q, "Wo wohnt Astrid Lindgrens Titelheldin Lotta?");
strcpy_s(m1.a, "H�llenl�rmgasse");
strcpy_s(m1.b, "Krachmacherstra�e");
strcpy_s(m1.c, "Donnerschlagallee");
strcpy_s(m1.d, "Radaubruderweg");
strcpy_s(m1.ans, "B");

// Frage m2
strcpy_s(m2.q, "Um wen geht es im Namen der Band Frankie Goes To Hollywood?");
strcpy_s(m2.a, "Frank Capra");
strcpy_s(m2.b, "Frank Sinatra");
strcpy_s(m2.c, "Franklin D Roosevelt");
strcpy_s(m2.d, "Frank Zappa");
strcpy_s(m2.ans, "B");

// Frage m3
strcpy_s(m3.q, "Am Grund tiefer Seen ist das Wasser ...?");
strcpy_s(m3.a, "nie k�lter als 4�C");
strcpy_s(m3.b, "sauerstofffrei");
strcpy_s(m3.c, "nicht trinkbar");
strcpy_s(m3.d, "lichtundurchl�ssig");
strcpy_s(m3.ans, "A");

// Frage m4
strcpy_s(m4.q, "Was sieht aus wie ein Kolibri, ist aber ein Schmetterling?");
strcpy_s(m4.a, "Taubenschw�nzchen");
strcpy_s(m4.b, "Meisenfl�gelchen");
strcpy_s(m4.c, "Amselschn�belchen");
strcpy_s(m4.d, "G�nsefederchen");
strcpy_s(m4.ans, "A");

QuestionN n0, n1, n2, n3, n4;

// Frage n0
strcpy_s(n0.q, "Wo befindet sich der Hauptsitz der UNESCO?");
strcpy_s(n0.a, "Br�ssel");
strcpy_s(n0.b, "Paris");
strcpy_s(n0.c, "London");
strcpy_s(n0.d, "Helsinki");
strcpy_s(n0.ans, "B");

// Frage n1
strcpy_s(n1.q, "Welche Weltreligion kennt keine Schutzengel?");
strcpy_s(n1.a, "Judentum");
strcpy_s(n1.b, "Islam");
strcpy_s(n1.c, "Christentum");
strcpy_s(n1.d, "Buddhismus");
strcpy_s(n1.ans, "D");

// Frage n2
strcpy_s(n2.q, "Gegen wen setzte sich Bonn 1949 bei der Wahl zur Bundeshauptstadt mit 33:29 Stimmen durch?");
strcpy_s(n2.a, "Kassel");
strcpy_s(n2.b, "M�nchen");
strcpy_s(n2.c, "Frankfurt am Main");
strcpy_s(n2.d, "Westberlin");
strcpy_s(n2.ans, "C");

// Frage n3
strcpy_s(n3.q, "Giraffe, Pfau und Eidechse sind ...?");
strcpy_s(n3.a, "lebend geb�rend");
strcpy_s(n3.b, "heilige Tiere in Indien");
strcpy_s(n3.c, "Tischler-Werkzeuge");
strcpy_s(n3.d, "Sternbilder");
strcpy_s(n3.ans, "D");

// Frage n4
strcpy_s(n4.q, "Wie hei�t die zweitgr��te Stadt Gro�britanniens?");
strcpy_s(n4.a, "Birmingham");
strcpy_s(n4.b, "Manchester");
strcpy_s(n4.c, "Liverpool");
strcpy_s(n4.d, "Glasgow");
strcpy_s(n4.ans, "A");

QuestionO o0, o1, o2, o3, o4;

// Frage o0
strcpy_s(o0.q, "Mit wem stand Edmund Hillary 1953 auf dem Gipfel des Mount Everest?");
strcpy_s(o0.a, "Nasreddin Hodscha");
strcpy_s(o0.b, "Nursay Pimsorn");
strcpy_s(o0.c, "Tenzing Norgay");
strcpy_s(o0.d, "Abrindranath Singh");
strcpy_s(o0.ans, "C");

// Frage o1
strcpy_s(o1.q, "Welche beiden Gibb-Br�der der Popband The Bee Gees sind Zwillinge?");
strcpy_s(o1.a, "Robin und Barry");
strcpy_s(o1.b, "Maurice und Robin");
strcpy_s(o1.c, "Barry und Maurice");
strcpy_s(o1.d, "Andy und Robin");
strcpy_s(o1.ans, "B");

// Frage o2
strcpy_s(o2.q, "Welcher ber�hmte Schriftsteller erbaute als diplomierter Architekt ein Freibad in Z�rich?");
strcpy_s(o2.a, "Joseph Roth");
strcpy_s(o2.b, "Martin Walser");
strcpy_s(o2.c, "Max Frisch");
strcpy_s(o2.d, "Friedrich D�rrenmatt");
strcpy_s(o2.ans, "C");

// Frage o3
strcpy_s(o3.q, "Wer bekam 1954 den Chemie- und 1962 den Friedensnobelpreis?");
strcpy_s(o3.a, "Linus Pauling");
strcpy_s(o3.b, "Otto Hahn");
strcpy_s(o3.c, "Pearl S Buck");
strcpy_s(o3.d, "Albert Schweitzer");
strcpy_s(o3.ans, "A");

// Frage o4
strcpy_s(o4.q, "Welches chemische Element macht mehr als die H�lfte der Masse eines menschlichen K�rpers aus?");
strcpy_s(o4.a, "Kohlenstoff");
strcpy_s(o4.b, "Kalzium");
strcpy_s(o4.c, "Sauerstoff");
strcpy_s(o4.d, "Eisen");
strcpy_s(o4.ans, "C");




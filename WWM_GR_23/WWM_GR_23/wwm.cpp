// wwm.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stdafx.h"
#include "types.h"									// struct question & typedef struct question Question

// EXTERNE FUNKTIONEN
void drawgui();										// Generiert die Oberfl�che (gui.cpp)
void printLevels();
void printQ(int level, Question question);			// zeigt Frage und Antworten an (gui.cpp)
void randomQ(int level);							// chooses random question (gui.cpp)
void questionDB();									// Generiert die Fragen-Datenbank (questions.cpp)
void handleClicks(int level);						// verarbeitet Mausklicks (gui.cpp)
void printGameOver(char choice);					// Anzeige bei falscher Antwort (gui.cpp)

// EXTERNE VARIABLEN
// extern Question questions[15][5];
extern int qPosX, qPosY;
extern int jPosY;
extern AnsButton buttonA, buttonB, buttonC, buttonD;

// Jokerstrukturen initialisieren
extern int jFiftyPosX, jTelPosX, jAudPosX, jSpecialPosX;
Joker jFifty = { 'f', 0 , jFiftyPosX };			// 50/50-Joker
Joker jTel = { 't', 0, jTelPosX };				// Telefonjoker
Joker jAud = { 'a', 0, jAudPosX };				// Publikumjoker
Joker jSpecial = { 's', 0, jSpecialPosX };		// Zusatzjoker
Joker jokers[4] = { jFifty, jTel, jAud, jSpecial };

// VARIABLEN
int level = 0;				// aktuelle Spielstufe
int gameOver = 0;			// 
Question qID;				// aktuelle Frage







// FUNCTION CHECKANS
// Vergleicht die gegebene Antwort (choice) mit der richtigen Antwort (correctAns)
// R�ckgabewert 1 bei korrekter Antwort, 0 bei falscher Antwort
int checkAns(char choice, char correctAns) {
	if (choice == correctAns) {
		if (level == 14) {									// Letzte Frage?
			text2(qPosX, qPosY, "Spiel gewonnen!");
			printf("Game won!\n");
		}
		else {
			printf("Antwort %c ist korrekt!\n\n", choice);
			randomQ(++level);								// n�chste Gewinnstufe & neue Frage
		};
		return 1;
	}
	else {
		printGameOver(choice);
		gameOver = 1;
		return 0;
	};
};






int main() {

	// Spieloberfl�che zeichnen
	printf("Drawing GUI... ");
	drawgui();
	printLevels();
	printf("done.\n");

	// Fragendatenbank generieren
	printf("Generating question database... ");
	questionDB();
	printf("done.\n");

	// erste zuf�llige Frage aufrufen
	printf("Random question...\n\n");
	randomQ(level);

	// Mausklicks verarbeiten
	handleClicks(level);

	// printf("jFanswers: %c, %c\n", jFanswers[0], jFanswers[1]);

	getchar();
	return 0;
}


#include <stdio.h>
#include <stdlib.h>
#include "stdafx.h"
#include <string.h>
#include "types.h"

Question a0, a1, a2, a3, a4, b0, b1, b2, b3, b4, c0, c1, c2, c3, c4, d0, d1, d2, d3, d4, e0, e1, e2, e3, e4, f0, f1, f2, f3, f4, g0, g1, g2, g3, g4, h0, h1, h2, h3, h4, i0, i1, i2, i3, i4, j0, j1, j2, j3, j4, k0, k1, k2, k3, k4, l0, l1, l2, l3, l4, m0, m1, m2, m3, m4,
n0, n1, n2, n3, n4, o0, o1, o2, o3, o4;

void questionDB() {

	// Frage a0
	strcpy_s(a0.q, 100, "Peter Kreuder komponierte 1936 den Schlager 'Ich wollt' ich wär' ein ...'?");
	strcpy_s(a0.a, 30, "Hund");
	strcpy_s(a0.b, 30, "Huhn");
	strcpy_s(a0.c, 30, "Hummer");
	strcpy_s(a0.d, 30, "Huflattich");
	a0.ans = 'B';

	// Frage a1
	strcpy_s(a1.q, 100, "Wie nennt man großen Wirbel um eine berühmte Persönlichkeit?");
	strcpy_s(a1.a, 30, "Prominentenkirmes");
	strcpy_s(a1.b, 30, "Starrummel");
	strcpy_s(a1.c, 30, "VIP-Jahrmarkt");
	strcpy_s(a1.d, 30, "Götterzirkus");
	a1.ans = 'B';

	// Frage a2
	strcpy_s(a2.q, 100, "Was kann man essen?");
	strcpy_s(a2.a, 30, "Aktenkoffer");
	strcpy_s(a2.b, 30, "Schulranzen");
	strcpy_s(a2.c, 30, "Rucksack");
	strcpy_s(a2.d, 30, "Apfeltasche");
	a2.ans = 'D';

	// Frage a3
	strcpy_s(a3.q, 100, "Im Gegensatz zur Spinne ist die Fliege ein ...?");
	strcpy_s(a3.a, 30, "Inbier");
	strcpy_s(a3.b, 30, "Inschnaps");
	strcpy_s(a3.c, 30, "Inwein");
	strcpy_s(a3.d, 30, "Insekt");
	a3.ans = 'D';

	// Frage a4
	strcpy_s(a4.q, 100, "Wobei handelt es sich nicht um ein Reptil?");
	strcpy_s(a4.a, 30, "Wiesenotter");
	strcpy_s(a4.b, 30, "Kreuzotter");
	strcpy_s(a4.c, 30, "Puffotter");
	strcpy_s(a4.d, 30, "Eidotter");
	a4.ans = 'D';



	// Question b0, b1, b2, b3, b4;

	// Frage b0
	strcpy_s(b0.q, 100, "Wobei handelt es sich um ein Notsignal im internationalen Funkverkehr?");
	strcpy_s(b0.a, 30, "Mayday");
	strcpy_s(b0.b, 30, "Down Town");
	strcpy_s(b0.c, 30, "Jetset");
	strcpy_s(b0.d, 30, "Flower Power");
	b0.ans = 'A';

	// Frage b1
	strcpy_s(b1.q, 100, "Was steht über dem Haupteingang des Reichstagsgebäudes in Berlin?");
	strcpy_s(b1.a, 30, "Dem deutschen Volke");
	strcpy_s(b1.b, 30, "Achtung, Bundestag!");
	strcpy_s(b1.c, 30, "Nur für Abgeordnete");
	strcpy_s(b1.d, 30, "Bitte nicht stören!");
	b1.ans = 'A';

	// Frage b2
	strcpy_s(b2.q, 100, "Kleine Sünden straft der liebe Gott dem Volksmund nach ...?");
	strcpy_s(b2.a, 30, "sofort");
	strcpy_s(b2.b, 30, "so gut wie nie");
	strcpy_s(b2.c, 30, "mit Fernsehverbot");
	strcpy_s(b2.d, 30, "in neun Monaten");
	b2.ans = 'A';

	// Frage b3
	strcpy_s(b3.q, 100, "Welches Imitat erfüllt sichtlich seinen Zweck?");
	strcpy_s(b3.a, 30, "Tomatenröter");
	strcpy_s(b3.b, 30, "Kaffeeweißer");
	strcpy_s(b3.c, 30, "Senfgelber");
	strcpy_s(b3.d, 30, "Schokoladenbräuner");
	b3.ans = 'B';

	// Frage b4
	strcpy_s(b4.q, 100, "Für Touristen aus aller Welt liegt Bad Kreuznach auf jeden Fall ...?");
	strcpy_s(b4.a, 30, "ganz nah");
	strcpy_s(b4.b, 30, "in der Nähe");
	strcpy_s(b4.c, 30, "an der Nahe");
	strcpy_s(b4.d, 30, "in weiter Ferne so nah");
	b4.ans = 'C';



	// Question c0, c1, c2, c3, c4;

	// Frage c0
	strcpy_s(c0.q, 100, "Wie heißt - laut einem Märchen der Brüder Grimm - die Schwester von Schneeweißchen?");
	strcpy_s(c0.a, 30, "Fliederlila");
	strcpy_s(c0.b, 30, "Maisgelb");
	strcpy_s(c0.c, 30, "Rosenrot");
	strcpy_s(c0.d, 30, "Kornblumenblau");
	c0.ans = 'C';

	// Frage c1
	strcpy_s(c1.q, 100, "Wer davonläuft, der gibt einer Redewendung nach ...?");
	strcpy_s(c1.a, 30, "Wechselgeld");
	strcpy_s(c1.b, 30, "Fersengeld");
	strcpy_s(c1.c, 30, "Kleingeld");
	strcpy_s(c1.d, 30, "Kindergeld");
	c1.ans = 'B';

	// Frage c2
	strcpy_s(c2.q, 100, "Eine populäre Spielart der amerikanischen Volksmusik heißt Country & ...?");
	strcpy_s(c2.a, 30, "Thriller");
	strcpy_s(c2.b, 30, "Comedy");
	strcpy_s(c2.c, 30, "Slapstick");
	strcpy_s(c2.d, 30, "Western");
	c2.ans = 'D';

	// Frage c3
	strcpy_s(c3.q, 100, "Welchen Ausdruck prägte der Philosoph Hegel?");
	strcpy_s(c3.a, 30, "für sich und an");
	strcpy_s(c3.b, 30, "und für an sich");
	strcpy_s(c3.c, 30, "an und für sich");
	strcpy_s(c3.d, 30, "sich und für an");
	c3.ans = 'C';

	// Frage c4
	strcpy_s(c4.q, 100, "Wie nennt man die direkte Verbindung von Plus- und Minuspol einer Spannungsquelle?");
	strcpy_s(c4.a, 30, "Glasbruch");
	strcpy_s(c4.b, 30, "Wasserschaden");
	strcpy_s(c4.c, 30, "Rohrverstopfung");
	strcpy_s(c4.d, 30, "Kurzschluss");
	c4.ans = 'D';



	// Question d0, d1, d2, d3, d4;

	// Frage d0
	strcpy_s(d0.q, 100, "Wer hat von Berufs wegen mit dem Spritzenhaus zu tun?");
	strcpy_s(d0.a, 30, "Gärtner");
	strcpy_s(d0.b, 30, "Krankenschwester");
	strcpy_s(d0.c, 30, "Feuerwehrmann");
	strcpy_s(d0.d, 30, "Konditor");
	d0.ans = 'C';

	// Frage d1
	strcpy_s(d1.q, 100, "Durch welches Verfahren schickte man im alten Athen seine Mitbürger in die Verbannung?");
	strcpy_s(d1.a, 30, "Götterspeise");
	strcpy_s(d1.b, 30, "Henkersmahlzeit");
	strcpy_s(d1.c, 30, "Scherbengericht");
	strcpy_s(d1.d, 30, "Grillteller");
	d1.ans = 'C';

	// Frage d2
	strcpy_s(d2.q, 100, "Vor welchen gesetzlosen Gesellen fürchteten sich schon die alten Rittersleut'?");
	strcpy_s(d2.a, 30, "Gebüschganoven");
	strcpy_s(d2.b, 30, "Heckenstrolche");
	strcpy_s(d2.c, 30, "Baumbanditen");
	strcpy_s(d2.d, 30, "Strauchdiebe");
	d2.ans = 'D';

	// Frage d3
	strcpy_s(d3.q, 100, "Wo befindet sich das Orchester während einer Opernaufführung?");
	strcpy_s(d3.a, 30, "Fahrradweg");
	strcpy_s(d3.b, 30, "Grünstreifen");
	strcpy_s(d3.c, 30, "Böschung");
	strcpy_s(d3.d, 30, "Graben");
	d3.ans = 'D';

	// Frage d4
	strcpy_s(d4.q, 100, "Nicht nur Ross und Reiter brauchen zum Sieg oft ein gutes ...?");
	strcpy_s(d4.a, 30, "Shwedish");
	strcpy_s(d4.b, 30, "Denish");
	strcpy_s(d4.c, 30, "Finish");
	strcpy_s(d4.d, 30, "Norvegish");
	d4.ans = 'C';



	// Question e0, e1, e2, e3, e4;

	// Frage e0
	strcpy_s(e0.q, 100, "Was ist die Amtssprache von Mexiko?");
	strcpy_s(e0.a, 30, "Portugiesisch");
	strcpy_s(e0.b, 30, "Englisch");
	strcpy_s(e0.c, 30, "Französisch");
	strcpy_s(e0.d, 30, "Spanisch");
	e0.ans = 'D';

	// Frage e1
	strcpy_s(e1.q, 100, "Wie nennt man ein gedrucktes Rundschreiben des Papstes?");
	strcpy_s(e1.a, 30, "Enzephalitis");
	strcpy_s(e1.b, 30, "Enzym");
	strcpy_s(e1.c, 30, "Enzyklopädie");
	strcpy_s(e1.d, 30, "Enzyklika");
	e1.ans = 'D';

	// Frage e2
	strcpy_s(e2.q, 100, "Was wird nicht durch Reflexe ausgelöst?");
	strcpy_s(e2.a, 30, "Gähnen");
	strcpy_s(e2.b, 30, "Niesen");
	strcpy_s(e2.c, 30, "Kauen");
	strcpy_s(e2.d, 30, "Schlucken");
	e2.ans = 'C';

	// Frage e3
	strcpy_s(e3.q, 100, "Welches Wort bedeutet so viel wie 'allgemein verbreitet' oder 'üblich'?");
	strcpy_s(e3.a, 30, "bodenständig");
	strcpy_s(e3.b, 30, "geländegängig");
	strcpy_s(e3.c, 30, "landläufig");
	strcpy_s(e3.d, 30, "stadtflüchtig");
	e3.ans = 'C';

	// Frage e4
	strcpy_s(e4.q, 100, "Für Kosmetika und als Nahrungsergänzungsmittel verwendet man das Öl der ...?");
	strcpy_s(e4.a, 30, "Wunderkerze");
	strcpy_s(e4.b, 30, "Nachtkerze");
	strcpy_s(e4.c, 30, "Taufkerze");
	strcpy_s(e4.d, 30, "Zündkerze");
	e4.ans = 'B';



	// Question f0, f1, f2, f3, f4;

	// Frage f0
	strcpy_s(f0.q, 100, "Unter welchem Namen sangen Wigald Boning und Olli Dittrich 'Lieder, die die Welt nicht braucht'?");
	strcpy_s(f0.a, 30, "Die Behämmerten");
	strcpy_s(f0.b, 30, "Die Abgedrehten");
	strcpy_s(f0.c, 30, "Die Doofen");
	strcpy_s(f0.d, 30, "Die Irren");
	f0.ans = 'C';

	// Frage f1
	strcpy_s(f1.q, 100, "Wer sang sich 1974 mit 'Hoch auf dem gelben Wagen' in die Charts?");
	strcpy_s(f1.a, 30, "Genscher");
	strcpy_s(f1.b, 30, "Scheel");
	strcpy_s(f1.c, 30, "Brandt");
	strcpy_s(f1.d, 30, "Kinkel");
	f1.ans = 'B';

	// Frage f2
	strcpy_s(f2.q, 100, "Mit den Worten 'Der Weltraum, unendliche Weiten' beginnen die Abenteuer des Raumschiffes ...?");
	strcpy_s(f2.a, 30, "Galactica");
	strcpy_s(f2.b, 30, "Orion");
	strcpy_s(f2.c, 30, "Nostromo");
	strcpy_s(f2.d, 30, "Enterprise");
	f2.ans = 'D';

	// Frage f3
	strcpy_s(f3.q, 100, "Der potenzielle Bundespräsident (2004) Horst Köhler war davor Chef des ...?");
	strcpy_s(f3.a, 30, "IWF");
	strcpy_s(f3.b, 30, "DRK");
	strcpy_s(f3.c, 30, "IOC");
	strcpy_s(f3.d, 30, "DFB");
	f3.ans = 'A';

	// Frage f4
	strcpy_s(f4.q, 100, "Welches Ministerium wird umgangssprachlich als Hardthöhe bezeichnet?");
	strcpy_s(f4.a, 30, "Justiz");
	strcpy_s(f4.b, 30, "Finanzen");
	strcpy_s(f4.c, 30, "Verteidigung");
	strcpy_s(f4.d, 30, "Gesundheit");
	f4.ans = 'C';



	// Question g0, g1, g2, g3, g4;

	// Frage g0
	strcpy_s(g0.q, 100, "Was ist ein Rebus?");
	strcpy_s(g0.a, 30, "Kreditinstitut");
	strcpy_s(g0.b, 30, "Werkzeug");
	strcpy_s(g0.c, 30, "Weinstock");
	strcpy_s(g0.d, 30, "Bilderrätsel");
	g0.ans = 'D';

	// Frage g1
	strcpy_s(g1.q, 100, "Wie heißt der König der Erdmännchen in der Augsburger Puppenkiste?");
	strcpy_s(g1.a, 30, "Kalle Wirsch");
	strcpy_s(g1.b, 30, "Klein Zack");
	strcpy_s(g1.c, 30, "Zwerg Nase");
	strcpy_s(g1.d, 30, "Frodo Beutlin");
	g1.ans = 'A';

	// Frage g2
	strcpy_s(g2.q, 100, "Wer gehört nicht zu Dorothys Begleitern in 'Der Zauberer von Oz'?");
	strcpy_s(g2.a, 30, "fast kopfloser Nick");
	strcpy_s(g2.b, 30, "Vogelscheuche");
	strcpy_s(g2.c, 30, "Zinnmann");
	strcpy_s(g2.d, 30, "ängstlicher Löwe");
	g2.ans = 'A';

	// Frage g3
	strcpy_s(g3.q, 100, "Womit bescherte Songwriter Leonard Cohen der Welt einen Klassiker?");
	strcpy_s(g3.a, 30, "Suzanne");
	strcpy_s(g3.b, 30, "Michelle");
	strcpy_s(g3.c, 30, "Rosanna");
	strcpy_s(g3.d, 30, "Lucille");
	g3.ans = 'A';

	// Frage g4
	strcpy_s(g4.q, 100, "Was kann man zum Lösen festgerosteter Schrauben verwenden?");
	strcpy_s(g4.a, 30, "Kriechöl");
	strcpy_s(g4.b, 30, "Schleichkerosin");
	strcpy_s(g4.c, 30, "Trödelbenzin");
	strcpy_s(g4.d, 30, "Bummeldiesel");
	g4.ans = 'A';



	// Question h0, h1, h2, h3, h4;

	// Frage h0
	strcpy_s(h0.q, 100, "Wie nennt man von Gletschern transportierten Gesteinsschutt?");
	strcpy_s(h0.a, 30, "Moräne");
	strcpy_s(h0.b, 30, "Muräne");
	strcpy_s(h0.c, 30, "Morelle");
	strcpy_s(h0.d, 30, "Murnau");
	h0.ans = 'A';

	// Frage h1
	strcpy_s(h1.q, 100, "Wer schoss in 62 Fußball-Länderspielen 68 Tore?");
	strcpy_s(h1.a, 30, "Gerd Müller");
	strcpy_s(h1.b, 30, "Horst Hrubesch");
	strcpy_s(h1.c, 30, "Helmut Rahn");
	strcpy_s(h1.d, 30, "Jupp Heynckes");
	h1.ans = 'A';

	// Frage h2
	strcpy_s(h2.q, 100, "Wie heißt ein Kanton der Schweiz?");
	strcpy_s(h2.a, 30, "Baden-Baden");
	strcpy_s(h2.b, 30, "Karlsruhe");
	strcpy_s(h2.c, 30, "Konstanz");
	strcpy_s(h2.d, 30, "Freiburg");
	h2.ans = 'D';

	// Frage h3
	strcpy_s(h3.q, 100, "Eine Shakespeare-Komödie trägt den Titel 'Wie es euch ...'?");
	strcpy_s(h3.a, 30, "gefällt");
	strcpy_s(h3.b, 30, "beliebt");
	strcpy_s(h3.c, 30, "genehm ist");
	strcpy_s(h3.d, 30, "in den Kram passt");
	h3.ans = 'A';

	// Frage h4
	strcpy_s(h4.q, 100, "Ein Stummfilmklassiker von Sergej Eisenstein heißt 'Panzerkreuzer ...'?");
	strcpy_s(h4.a, 30, "Rasputin");
	strcpy_s(h4.b, 30, "Potemkin");
	strcpy_s(h4.c, 30, "Iljuschin");
	strcpy_s(h4.d, 30, "Putin");
	h4.ans = 'B';



	// Question i0, i1, i2, i3, i4;

	// Frage i0
	strcpy_s(i0.q, 100, "Welches deutsche Fürstenhaus organisierte bis ins 19. Jahrhundert die kaiserliche Reichspost?");
	strcpy_s(i0.a, 30, "Fürstenberg");
	strcpy_s(i0.b, 30, "Schaumburg-Lippe");
	strcpy_s(i0.c, 30, "Hohenlohe");
	strcpy_s(i0.d, 30, "Thurn und Taxis");
	i0.ans = 'D';

	// Frage i1
	strcpy_s(i1.q, 100, "Was ist das so genannte Horn von Afrika?");
	strcpy_s(i1.a, 30, "Sternbild");
	strcpy_s(i1.b, 30, "Halbinsel");
	strcpy_s(i1.c, 30, "fossiles Kultobjekt");
	strcpy_s(i1.d, 30, "Berg in Tansania");
	i1.ans = 'B';

	// Frage i2
	strcpy_s(i2.q, 100, "Vor allem in Norddeutschland schwören traditionsbewusste Genießer auf 'Birnen, ...'?");
	strcpy_s(i2.a, 30, "Hering und Ei");
	strcpy_s(i2.b, 30, "Bohnen und Speck");
	strcpy_s(i2.c, 30, "Grünkohl und Senf");
	strcpy_s(i2.d, 30, "Klöße und Quark");
	i2.ans = 'B';

	// Frage i3
	strcpy_s(i3.q, 100, "Bei den Olympischen Spielen 1900 gab es einen Wettbewerb im Schießen auf lebende ...?");
	strcpy_s(i3.a, 30, "Hasen");
	strcpy_s(i3.b, 30, "Ratten");
	strcpy_s(i3.c, 30, "Fasane");
	strcpy_s(i3.d, 30, "Tauben");
	i3.ans = 'D';

	// Frage i4
	strcpy_s(i4.q, 100, "Wenn über die Sangesversuche von K-Fed berichtet wird, geht es um den Ehemann von ...?");
	strcpy_s(i4.a, 30, "Jennifer Lopez");
	strcpy_s(i4.b, 30, "Beyoncé Knowles");
	strcpy_s(i4.c, 30, "Christina Aguilera");
	strcpy_s(i4.d, 30, "Britney Spears");
	i4.ans = 'D';



	// Question j0, j1, j2, j3, j4;

	// Frage j0
	strcpy_s(j0.q, 100, "Wobei handelt es sich nicht um ein Insekt?");
	strcpy_s(j0.a, 30, "Stechmücke");
	strcpy_s(j0.b, 30, "Wintermücke");
	strcpy_s(j0.c, 30, "Grasmücke");
	strcpy_s(j0.d, 30, "Kriebelmücke");
	j0.ans = 'C';

	// Frage j1
	strcpy_s(j1.q, 100, "In welchem Hitchcock-Film kann die Protagonistin es nicht ertragen, die Farbe Rot zu sehen?");
	strcpy_s(j1.a, 30, "Marnie");
	strcpy_s(j1.b, 30, "Vertigo");
	strcpy_s(j1.c, 30, "Der unsichtbare Dritte");
	strcpy_s(j1.d, 30, "Die Vögel");
	j1.ans = 'A';

	// Frage j2
	strcpy_s(j2.q, 100, "Wie heißt eine Klasse der Wirbeltiere mit mehr als 20.000 bekannten Arten?");
	strcpy_s(j2.a, 30, "Schuppenfische");
	strcpy_s(j2.b, 30, "Kiemenfische");
	strcpy_s(j2.c, 30, "Knochenfische");
	strcpy_s(j2.d, 30, "Flossenfische");
	j2.ans = 'C';

	// Frage j3
	strcpy_s(j3.q, 100, "Wohin muss man reisen, wenn man die Überreste der antiken Metropole Pergamon sehen will?");
	strcpy_s(j3.a, 30, "Türkei");
	strcpy_s(j3.b, 30, "Ägypten");
	strcpy_s(j3.c, 30, "Zypern");
	strcpy_s(j3.d, 30, "Kreta");
	j3.ans = 'A';

	// Frage j4
	strcpy_s(j4.q, 100, "Wo machte man Mitte des 20. Jh. einen für das Bibelverständnis spektakulären Schriftrollen-Fund?");
	strcpy_s(j4.a, 30, "Qumran");
	strcpy_s(j4.b, 30, "Chichén Itzá");
	strcpy_s(j4.c, 30, "Angkor Vat");
	strcpy_s(j4.d, 30, "Herculaneum");
	j4.ans = 'A';



	// Question k0, k1, k2, k3, k4;

	// Frage k0
	strcpy_s(k0.q, 100, "Wie heißt das traditionelle Pferderennen in Siena?");
	strcpy_s(k0.a, 30, "Calcio");
	strcpy_s(k0.b, 30, "Palio");
	strcpy_s(k0.c, 30, "Barolo");
	strcpy_s(k0.d, 30, "Boccia");
	k0.ans = 'B';

	// Frage k1
	strcpy_s(k1.q, 100, "Nach einem Stadtteil welcher Metropole benannte sich der Regisseur Rosa von Praunheim?");
	strcpy_s(k1.a, 30, "Magdeburg");
	strcpy_s(k1.b, 30, "Frankfurt am Main");
	strcpy_s(k1.c, 30, "Hamburg");
	strcpy_s(k1.d, 30, "Köln");
	k1.ans = 'B';

	// Frage k2
	strcpy_s(k2.q, 100, "Wer war nie Kaiser des Heiligen Römischen Reiches");
	strcpy_s(k2.a, 30, "Ludwig der Bayer");
	strcpy_s(k2.b, 30, "Heinrich der Löwe");
	strcpy_s(k2.c, 30, "Otto der Große");
	strcpy_s(k2.d, 30, "Friedrich Barbarossa");
	k2.ans = 'B';

	// Frage k3
	strcpy_s(k3.q, 100, "Wie nennen Mathematiker einen Grundsatz, der nicht von anderen Sätzen abgeleitet werden kann?");
	strcpy_s(k3.a, 30, "Idiom");
	strcpy_s(k3.b, 30, "Axiom");
	strcpy_s(k3.c, 30, "Algorithmus");
	strcpy_s(k3.d, 30, "Hypothese");
	k3.ans = 'B';

	// Frage k4
	strcpy_s(k4.q, 100, "Welcher dieser vier Vornamen von Mozart wurde ins Lateinische übertragen zu 'Amadeus'?");
	strcpy_s(k4.a, 30, "Joannes");
	strcpy_s(k4.b, 30, "Chrysostomus");
	strcpy_s(k4.c, 30, "Wolfgangus");
	strcpy_s(k4.d, 30, "Theophilus");
	k4.ans = 'D';



	// Question l0, l1, l2, l3, l4;

	// Frage l0
	strcpy_s(l0.q, 100, "Was ist nach Mahón, dem Hauptort der Insel Menorca, benannt?");
	strcpy_s(l0.a, 30, "Marone");
	strcpy_s(l0.b, 30, "Mahagoni");
	strcpy_s(l0.c, 30, "Marihuana");
	strcpy_s(l0.d, 30, "Mayonnaise");
	l0.ans = 'D';

	// Frage l1
	strcpy_s(l1.q, 100, "Was ist kein Lamellenpilz?");
	strcpy_s(l1.a, 30, "Hallimasch");
	strcpy_s(l1.b, 30, "Champignon");
	strcpy_s(l1.c, 30, "Steinpilz");
	strcpy_s(l1.d, 30, "Knollenblätterpilz");
	l1.ans = 'C';

	// Frage l2
	strcpy_s(l2.q, 100, "Tabak zählt zur selben Pflanzenfamilie wie die ...?");
	strcpy_s(l2.a, 30, "Kartoffel");
	strcpy_s(l2.b, 30, "Karotte");
	strcpy_s(l2.c, 30, "Kamille");
	strcpy_s(l2.d, 30, "Kastanie");
	l2.ans = 'A';

	// Frage l3
	strcpy_s(l3.q, 100, "Wessen Romane wären nie veröffentlicht worden, wenn man seinem letzten Willen entsprochen hätte?");
	strcpy_s(l3.a, 30, "Hermann Hesse");
	strcpy_s(l3.b, 30, "Theodor Fontane");
	strcpy_s(l3.c, 30, "Franz Kafka");
	strcpy_s(l3.d, 30, "Robert Musil");
	l3.ans = 'C';

	// Frage l4
	strcpy_s(l4.q, 100, "Welcher Schriftsteller verfasste 'Die New York - Trilogie'?");
	strcpy_s(l4.a, 30, "John Irving");
	strcpy_s(l4.b, 30, "Paul Auster");
	strcpy_s(l4.c, 30, "Dan Brown");
	strcpy_s(l4.d, 30, "Ernest Hemmingway");
	l4.ans = 'B';



	// Question m0, m1, m2, m3, m4;

	// Frage m0
	strcpy_s(m0.q, 100, "Welcher deutsche Boxer schlug im Juni 1952 den Ringrichter Max Pippow zu Boden?");
	strcpy_s(m0.a, 30, "Peter Müller");
	strcpy_s(m0.b, 30, "Eckhard Dagge");
	strcpy_s(m0.c, 30, "Bubi Scholz");
	strcpy_s(m0.d, 30, "Max Schmeling");
	m0.ans = 'A';

	// Frage m1
	strcpy_s(m1.q, 100, "Wo wohnt Astrid Lindgrens Titelheldin Lotta?");
	strcpy_s(m1.a, 30, "Höllenlärmgasse");
	strcpy_s(m1.b, 30, "Krachmacherstraße");
	strcpy_s(m1.c, 30, "Donnerschlagallee");
	strcpy_s(m1.d, 30, "Radaubruderweg");
	m1.ans = 'B';

	// Frage m2
	strcpy_s(m2.q, 100, "Um wen geht es im Namen der Band Frankie Goes To Hollywood?");
	strcpy_s(m2.a, 30, "Frank Capra");
	strcpy_s(m2.b, 30, "Frank Sinatra");
	strcpy_s(m2.c, 30, "Franklin D Roosevelt");
	strcpy_s(m2.d, 30, "Frank Zappa");
	m2.ans = 'B';

	// Frage m3
	strcpy_s(m3.q, 100, "Am Grund tiefer Seen ist das Wasser ...?");
	strcpy_s(m3.a, 30, "nie kälter als 4°C");
	strcpy_s(m3.b, 30, "sauerstofffrei");
	strcpy_s(m3.c, 30, "nicht trinkbar");
	strcpy_s(m3.d, 30, "lichtundurchlässig");
	m3.ans = 'A';

	// Frage m4
	strcpy_s(m4.q, 100, "Was sieht aus wie ein Kolibri, ist aber ein Schmetterling?");
	strcpy_s(m4.a, 30, "Taubenschwänzchen");
	strcpy_s(m4.b, 30, "Meisenflügelchen");
	strcpy_s(m4.c, 30, "Amselschnäbelchen");
	strcpy_s(m4.d, 30, "Gänsefederchen");
	m4.ans = 'A';



	// Question n0, n1, n2, n3, n4;

	// Frage n0
	strcpy_s(n0.q, 100, "Wo befindet sich der Hauptsitz der UNESCO?");
	strcpy_s(n0.a, 30, "Brüssel");
	strcpy_s(n0.b, 30, "Paris");
	strcpy_s(n0.c, 30, "London");
	strcpy_s(n0.d, 30, "Helsinki");
	n0.ans = 'B';

	// Frage n1
	strcpy_s(n1.q, 100, "Welche Weltreligion kennt keine Schutzengel?");
	strcpy_s(n1.a, 30, "Judentum");
	strcpy_s(n1.b, 30, "Islam");
	strcpy_s(n1.c, 30, "Christentum");
	strcpy_s(n1.d, 30, "Buddhismus");
	n1.ans = 'D';

	// Frage n2
	strcpy_s(n2.q, 100, "Gegen wen setzte sich Bonn 1949 bei der Wahl zur Bundeshauptstadt mit 33:29 Stimmen durch?");
	strcpy_s(n2.a, 30, "Kassel");
	strcpy_s(n2.b, 30, "München");
	strcpy_s(n2.c, 30, "Frankfurt am Main");
	strcpy_s(n2.d, 30, "Westberlin");
	n2.ans = 'C';

	// Frage n3
	strcpy_s(n3.q, 100, "Giraffe, Pfau und Eidechse sind ...?");
	strcpy_s(n3.a, 30, "lebend gebärend");
	strcpy_s(n3.b, 30, "heilige Tiere in Indien");
	strcpy_s(n3.c, 30, "Tischler-Werkzeuge");
	strcpy_s(n3.d, 30, "Sternbilder");
	n3.ans = 'D';

	// Frage n4
	strcpy_s(n4.q, 100, "Wie heißt die zweitgrößte Stadt Großbritanniens?");
	strcpy_s(n4.a, 30, "Birmingham");
	strcpy_s(n4.b, 30, "Manchester");
	strcpy_s(n4.c, 30, "Liverpool");
	strcpy_s(n4.d, 30, "Glasgow");
	n4.ans = 'A';



	// QuestionO o0, o1, o2, o3, o4;

	// Frage o0
	strcpy_s(o0.q, 100, "Mit wem stand Edmund Hillary 1953 auf dem Gipfel des Mount Everest?");
	strcpy_s(o0.a, 30, "Nasreddin Hodscha");
	strcpy_s(o0.b, 30, "Nursay Pimsorn");
	strcpy_s(o0.c, 30, "Tenzing Norgay");
	strcpy_s(o0.d, 30, "Abrindranath Singh");
	o0.ans = 'C';

	// Frage o1
	strcpy_s(o1.q, 100, "Welche beiden Gibb-Brüder der Popband The Bee Gees sind Zwillinge?");
	strcpy_s(o1.a, 30, "Robin und Barry");
	strcpy_s(o1.b, 30, "Maurice und Robin");
	strcpy_s(o1.c, 30, "Barry und Maurice");
	strcpy_s(o1.d, 30, "Andy und Robin");
	o1.ans = 'B';

	// Frage o2
	strcpy_s(o2.q, 100, "Welcher berühmte Schriftsteller erbaute als diplomierter Architekt ein Freibad in Zürich?");
	strcpy_s(o2.a, 30, "Joseph Roth");
	strcpy_s(o2.b, 30, "Martin Walser");
	strcpy_s(o2.c, 30, "Max Frisch");
	strcpy_s(o2.d, 30, "Friedrich Dürrenmatt");
	o2.ans = 'C';

	// Frage o3
	strcpy_s(o3.q, 100, "Wer bekam 1954 den Chemie- und 1962 den Friedensnobelpreis?");
	strcpy_s(o3.a, 30, "Linus Pauling");
	strcpy_s(o3.b, 30, "Otto Hahn");
	strcpy_s(o3.c, 30, "Pearl S Buck");
	strcpy_s(o3.d, 30, "Albert Schweitzer");
	o3.ans = 'A';

	// Frage o4
	strcpy_s(o4.q, 100, "Welches chemische Element macht mehr als die Hälfte der Masse eines menschlichen Körpers aus?");
	strcpy_s(o4.a, 30, "Kohlenstoff");
	strcpy_s(o4.b, 30, "Kalzium");
	strcpy_s(o4.c, 30, "Sauerstoff");
	strcpy_s(o4.d, 30, "Eisen");
	o4.ans = 'C';
};
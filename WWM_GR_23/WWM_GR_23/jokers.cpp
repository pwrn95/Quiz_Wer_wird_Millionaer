#include <stdio.h>
#include <stdlib.h>
#include "stdafx.h"
#include <string.h>
#include "types.h"
#include <time.h>

// Externe Variablen
extern Joker jFifty, jTel, jAud, jSpecial;
extern Question qID;
extern AnsButton buttonA, buttonB, buttonC, buttonD;
extern int qPosX, qPosY;
// Externe Funktionen
void removeAns(ansButton button);
void printTelAns(char telAns[30]);
void histogram(int a, int b, int c, int d);


void useJ(Joker joker) {
	if (joker.ID == 'f') {				// WENN 50/50-JOKER GEW�HLT WURDE
		// ...
		if (qID.ans == 'A') {          // WENN A KORREKT WERDEN C UND D AUSGEBLENDET
			removeAns(buttonC);
			removeAns(buttonD);
		}
		else if (qID.ans == 'B') {     // WENN B KORREKT WERDEN C UND D AUSGEBLENDET
			removeAns(buttonC);
			removeAns(buttonD);
		}
		else if (qID.ans == 'C') {     // WENN C KORREKT WERDEN A UND B AUSGEBLENDET
			removeAns(buttonA);
			removeAns(buttonB);
		}
		else if (qID.ans == 'D') {     // WENN D KORREKT WERDEN A UND B AUSGEBLENDET
			removeAns(buttonA);
			removeAns(buttonB);
		}
	}

	else if (joker.ID == 't') {			// WENN TELEFON-JOKER GEW�HLT WURDE
		char telAns[50];
		srand(time(NULL));
		int TJOKER = rand() % 10 + 1;   // ERZEUGT ZUFALLSZAHL ZWISCHEN 1 UND 10
		if (TJOKER <= 7) {															// zeigt mit 70% die richtige L�sung an
			printf("Ich glaube, Antwort %c ist richtig. \n", qID.ans);
			sprintf_s(telAns, "Ich glaube, Antwort %c ist richtig", qID.ans);
		}
		else if (9 >= TJOKER > 7) {													// zeigt mit 20% an, dass der Joker die Antwort nicht wei�
			printf("Das wei� ich leider �berhaupt nicht. \n");
			strcpy_s(telAns, 50, "Das wei� ich leider �berhaupt nicht.");
		}
		else if (TJOKER > 9) {														// zeigt mit 10% immer Antwort B an, evtl falsch
			printf("Ich glaube, Antwort B ist richtig. \n");  
			strcpy_s(telAns, 50, "Ich glaube, Antwort B ist richtig.");
		}
		printTelAns(telAns);
	}

	else if (joker.ID == 'a') {			// WENN PUBLIKUMS-JOKER GEW�HLT WURDE
		// ...
		if (qID.ans == 'A') {
			histogram(53, 17, 11, 19);  //ZEIGT HISTOGRAMM F�R ANTWORT A AN
		}
		else if (qID.ans == 'B') {
			histogram(5, 45, 37, 13);   //ZEIGT HISTOGRAMM F�R ANTWORT B AN
		}
		else if (qID.ans == 'C') {
			histogram(13, 14, 65, 8);   //ZEIGT HISTOGRAMM F�R ANTWORT C AN
		}
		else if (qID.ans == 'D') {
			histogram(21, 22, 28, 29);   //ZEIGT HISTOGRAMM F�R ANTWORT D AN
		}
	}
	else if (joker.ID == 's') {			// WENN ZUSATZ-JOKER GEW�HLT WURDE
		// ...
		// ZUSATZJOKER = ZWEITE CHANCE?

	}
};
#pragma once

// FRAGEN
struct question {
	char q[100];		// Frage
	char a[30];			// Antwort A
	char b[30];			// Antwort B
	char c[30];			// Antwort C
	char d[30];			// Antwort D
	char ans;			// Buchstabe der korrekten Antwort 
};
typedef struct question Question;

// JOKER
struct joker {
	char ID;			// K�rzel des Jokers (f = 50/50, t = Telefonj., a = Publikumsj., s = Zusatzjoker)
	int used;			// Verf�gbarkeit des Joker (= 1 wenn er schon benutzt wurde)
	int xPos;			// X-Position auf GUI
};
typedef struct joker Joker;

// ANTWORT BUTTONS
struct ansButton {
	int x;
	int y;
	char letter;
	int vis;			// Sichtbarkeit
	int hitBoxX1;
	int hitBoxX2;
	int hitBoxY1;
	int hitBoxY2;
};
typedef struct ansButton AnsButton;
#include <stdio.h>
#include <stdlib.h>
#include "stdafx.h"
#include <string.h>
#include "types.h"
#include <time.h>

#define BOXCOLOUR1 0x6262b3 
#define LINECOLOUR 0xE6E6E6 

// SPIELFELDGR�SSE
int sm = 4;													// size multiplier
int sizeX = 30 * sm;										// horizontale Spielfeldgr��e
int sizeY = 15 * sm;										// vertikale Spielfeldgr��e
// Koordinaten der Joker auf dem Spielfeld
int jDistanceX = 5 * sm;									// horizontaler Abstand der Joker
int jPosY = 13 * sm;										// y-Koordinate der Joker
int jFiftyPosX = 3 * sm;									// x-Koordinate des ersten Jokers
int jTelPosX = jFiftyPosX + jDistanceX;
int jAudPosX = jFiftyPosX + 2 * jDistanceX;
int jSpecialPosX = jFiftyPosX + 3 * jDistanceX;
float jButtonSize = 1.2 * sm;								// Gr��e der Joker-Buttons
int jNameOffsetY = 2 * sm;									// Vertikaler Abstand der Joker-Namen
int hitBoxJArea = 1 * sm;
// Koordinaten der Frage auf dem Spielfeld
int qPosX = 11 * sm;
int qPosY = 8 * sm;
// Koordinaten der Antworten auf dem Spielfeld
int buttonsACposX = 1 * sm;
int buttonsBDposX = 12 * sm;
int buttonsABposY = 4 * sm;
int buttonsCDposY = 1 * sm;
int ansACposX = (buttonsACposX + buttonsBDposX) / 2;						// X-Koordinate der Antworten A & C
int ansBDposX = buttonsBDposX + (buttonsBDposX - buttonsACposX) / 2;		// X-Koordinate der Antworten B & D
// Hitbox Button A
int hitBoxX1A = 0 * sm;
int hitBoxX2A = 10 * sm;
int hitBoxY1A = 3 * sm;
int hitBoxY2A = 5 * sm;
// Hitbox Button B
int hitBoxX1B = 11 * sm;
int hitBoxX2B = 21 * sm;
int hitBoxY1B = 3 * sm;
int hitBoxY2B = 5 * sm;
// Hitbox Button C
int hitBoxX1C = 0 * sm;
int hitBoxX2C = 10 * sm;
int hitBoxY1C = 0 * sm;
int hitBoxY2C = 2 * sm;
// Hitbox Button D
int hitBoxX1D = 11 * sm;
int hitBoxX2D = 21 * sm;
int hitBoxY1D = 0 * sm;
int hitBoxY2D = 2 * sm;
AnsButton buttonA = { buttonsACposX, buttonsABposY, 'A', 1, hitBoxX1A, hitBoxX2A, hitBoxY1A, hitBoxY2A };
AnsButton buttonB = { buttonsBDposX, buttonsABposY, 'B', 1, hitBoxX1B, hitBoxX2B, hitBoxY1B, hitBoxY2B };
AnsButton buttonC = { buttonsACposX, buttonsCDposY, 'C', 1, hitBoxX1C, hitBoxX2C, hitBoxY1C, hitBoxY2C };
AnsButton buttonD = { buttonsBDposX, buttonsCDposY, 'D', 1, hitBoxX1D, hitBoxX2D, hitBoxY1D, hitBoxY2D };
float ansButtonSize = 0.8 * sm;
// Histogramm
int barPosX = 23 * sm;

// Vertikale Trennlinie
int vLinePosX = 22 * sm;
int qNoPosX = 23 * sm;
int levelIndPosX = 24 * sm;									// x-Koordinate der wei�en Rauten zur Anzeige der aktuellen Gewinnstufe
float levelIndSize = 0.2 * sm;
int levelMoneyPosX = 27 * sm;

// Horizontale Trennlinien
struct { int x; int y; float size; } hl1 = { 11 * sm, 9 * sm, 11 * sm };	// obere
struct { int x; int y; float size; } hl2 = { 11 * sm, 6 * sm, 11 * sm };	// untere


int i, xi, yi;

extern int gameOver;
extern Question qID;
extern Question a0, a1, a2, a3, a4, b0, b1, b2, b3, b4, c0, c1, c2, c3, c4, d0, d1, d2, d3, d4, e0, e1, e2, e3, e4, f0, f1, f2, f3, f4, g0, g1, g2, g3, g4, h0, h1, h2, h3, h4, i0, i1, i2, i3, i4, j0, j1, j2, j3, j4, k0, k1, k2, k3, k4, l0, l1, l2, l3, l4, m0, m1, m2, m3, m4,
n0, n1, n2, n3, n4, o0, o1, o2, o3, o4;






extern Joker jFifty, jTel, jAud, jSpecial;
extern Joker jokers[4];




int redraw = 0;
int resetAns = 0;



extern int level;





// EXTERNE FUNKTIONEN
extern int checkAns(char choice, char correctAns);			// vergleicht die gegebene Antwort (choice) mit der richtigen Antwort (correctAns)
extern void useJ(Joker joker);		// f�hrt Joker aus (jokers.cpp)





// FUNKTIONSDEFINITIONEN:
void printLevels();

// FUNKTION drawgui
// erstellt die grafische Spieloberfl�che
void drawgui() {
	loeschen();
	formen("none");
	groesse(sizeX, sizeY);
	rahmen(LINECOLOUR);

	// obere horizontale Trennlinie
	form2(0, hl1.y, "-");
	symbolGroesse2(0, hl1.y, 0.9);
	farbe2(0, hl1.y, LINECOLOUR);
	form2(hl1.x, hl1.y, "-");
	symbolGroesse2(hl1.x, hl1.y, hl1.size);
	farbe2(hl1.x, hl1.y, LINECOLOUR);
	// untere horizontale Trennlinie
	form2(0, hl2.y, "-");
	symbolGroesse2(0, hl2.y, 0.9);
	farbe2(0, hl2.y, LINECOLOUR);
	form2(hl2.x, hl2.y, "-");
	symbolGroesse2(hl2.x, hl2.y, hl2.size);
	farbe2(hl2.x, hl2.y, LINECOLOUR);

	// JOKER 
	char jokerAbr[][6] = { "50/50", "T", "P", "Z" };
	Joker jokers[4] = { jFifty, jTel, jAud, jSpecial };
	char jokerName[][16] = { "50/50-Joker", "Telefon-Joker", "Publikums-Joker", "Zusatz-Joker" };
	for (i = 0; i < 4; i++) {
		int xi = jFiftyPosX + i * jDistanceX;				// x-Koordinate des Buttons berechnen
		form2(xi, jPosY, "c");
		symbolGroesse2(xi, jPosY, jButtonSize);
		if (jokers[i].used == 0) {
			farbe2(xi, jPosY, DARKBLUE);
		}
		else {
			farbe2(xi, jPosY, LIGHTGRAY);
		}
		text2(xi, jPosY, jokerAbr[i]);						// Beschriftung des Buttons
		text2(xi, jPosY - jNameOffsetY, jokerName[i]);		// Name des Jokers
	};

	// ANTWORT-BUTTONS MALEN
	AnsButton ansButtons[4] = { buttonA, buttonB, buttonC, buttonD };
	for (i = 0; i < 4; i++) {
		form2(ansButtons[i].x, ansButtons[i].y, "d");
		symbolGroesse2(ansButtons[i].x, ansButtons[i].y, ansButtonSize);
		farbe2(ansButtons[i].x, ansButtons[i].y, DARKBLUE);
		zeichen2(ansButtons[i].x, ansButtons[i].y, ansButtons[i].letter);
	};
};

// FUNKTION printLevels
// malt die Anzeige f�r den Spielstand
void printLevels() {
	char steps[][14] = { "50 EUR", "100 EUR", "200 EUR", "300 EUR", "500 EUR", "1.000 EUR", "2.000 EUR", "4.000 EUR", "8.000 EUR", "16.000 EUR", "32.000 EUR", "64.000 EUR", "125.000 EUR", "500.000 EUR", "1.000.000 EUR" };
	for (yi = 0, i = 0; yi < sizeY; yi++) {
		// vertikale Linie
		form2(vLinePosX, yi, " | ");
		farbe2(vLinePosX, yi, LINECOLOUR);
		if (yi % sm == 0) {
			// Nr. der Frage = y-Koordinate + 1
			char txt[3];
			snprintf(txt, 100, "%d", i + 1);
			// Raute
			form2(levelIndPosX, yi, "d");
			symbolGroesse2(levelIndPosX, yi, levelIndSize);
			farbe2(levelIndPosX, yi, WHITE);
			text2(qNoPosX, yi, txt);						// Nr. der Frage
			text2(levelMoneyPosX, yi, steps[i]);			// Gewinn der jeweiligen Stufe
			i++;
		}
	}
}

// FUNKTION printQ
// bekommt den Spielstand als int und die Frage als Question �bergeben
// zeigt Frage und Antworten an
void printQ(Question question) {
	if (redraw == 1) {		// Wenn die Oberfl�che neu gemalt werden muss
		redraw = 0;
		groesse(1, 1);
		drawgui();
		printLevels();
	}
	if (resetAns == 1) {	// alle Antworten wieder sichtbar machen
		resetAns = 0;
		farbe2(buttonA.x, buttonA.y, DARKBLUE);
		farbe2(buttonB.x, buttonB.y, DARKBLUE);
		farbe2(buttonC.x, buttonC.y, DARKBLUE);
		farbe2(buttonD.x, buttonD.y, DARKBLUE);
	}
	text2(qPosX, qPosY, question.q);				// Frage
	text2(ansACposX, buttonsABposY, question.a);	// Antwort A
	text2(ansBDposX, buttonsABposY, question.b);	// Antwort B
	text2(ansACposX, buttonsCDposY, question.c);	// Antwort C
	text2(ansBDposX, buttonsCDposY, question.d);	// Antwort D
	// ANZEIGE DER AKTUELLEN SPIELSTUFE
	if (level > 0) {
		farbe2(levelIndPosX, (level-1) * sm, WHITE);	// SYMBOL F�R VORIGE SPIELSTUFE WEISS F�RBEN
	}
	farbe2(levelIndPosX, level * sm, BOXCOLOUR1);		// SYMBOL F�R AKTUELLE SPIELSTUFE EINF�RBEN
};

// FUNKTION randomQ
// bekommt den Spielstand als int �bergeben
// w�hlt zuf�llig eine Frage der aktuellen Spielstufe (level) aus
void randomQ(int level) {
	// QUESTIONS ARRAY
	Question questions[15][5] = {
		{ a0, a1, a2, a3, a4 } ,
		{ b0, b1, b2, b3, b4 } ,
		{ c0, c1, c2, c3, c4 } ,
		{ d0, d1, d2, d3, d4 } ,
		{ e0, e1, e2, e3, e4 } ,
		{ f0, f1, f2, f3, f4 } ,
		{ g0, g1, g2, g3, g4 } ,
		{ h0, h1, h2, h3, h4 } ,
		{ i0, i1, i2, i3, i4 } ,
		{ j0, j1, j2, j3, j4 } ,
		{ k0, k1, k2, k3, k4 } ,
		{ l0, l1, l2, l3, l4 } ,
		{ m0, m1, m2, m3, m4 } ,
		{ n0, n1, n2, n3, n4 } ,
		{ o0, o1, o2, o3, o4 } ,
	};
	
	srand(time(NULL));								// rand-Funktion mit aktueller Uhrzeit initialisieren
	// ZUF�LLIGE FRAGE DER AKTUELLEN SPIELSTUFE
	qID = questions[level][rand() % 5];				// Zufallszahl 0 - 4, um eine der 5 Fragen der entsprechenden Stufe auszuw�hlen
	printQ(qID);								// Frage und Antworten mit printQ ausgeben
}

// FUNKTION removeJ
// bekommt einen Joker �bergeben
// graut den Joker auf der Spieloberfl�che aus
void removeJ(Joker joker) {
	farbe2(joker.xPos, jPosY, LIGHTGRAY);
}

// FUNKTION printGameOver
// bekommt den Buchstaben der gew�hlten Antwort als char �bergeben
// Zeigt auf der Spieloberfl�che und in der Konsole an, dass das Spiel verloren wurde.
void printGameOver(char choice) {
	printf("Answer %c is wrong!\n", choice);
	loeschen();
	text2(qPosX, qPosY, "Antwort leider falsch! Spiel vorbei.");
	printf("Game over.\n");
}

// FUNKTION printTelAns
// zeigt die Antwort des Telefonjokers an
void printTelAns(char telAns[50]) {
	text2(qPosX, qPosY + 2 * sm, telAns);
}

// FUNKTION handleClicks
// bekommt den Spielstand als int �bergeben
// Verarbeitung der Mausklicks
void handleClicks(int level) {
	int field, fieldX, fieldY;								// ANGEKLICKTES FELD AUF GUI (field: Feldnummer, fieldX: x-Koordinate, fieldY: y-Koordinate)

	do {
		char *msg = abfragen();
		if (strlen(msg) > 0) {
			if (msg[0] == '#') {
				sscanf_s(msg, "# %d %d %d", &field, &fieldX, &fieldY);
				// ANTWORT A
				if (fieldX >= hitBoxX1A && fieldX <= hitBoxX2A && fieldY >= hitBoxY1A && fieldY <= hitBoxY2A) {
					printf("Answer A selected. ");
					checkAns('A', qID.ans);					// ANTWORT �BERPR�FEN
				}
				// ANTWORT B
				else if (fieldX >= hitBoxX1B && fieldX <= hitBoxX2B && fieldY >= hitBoxY1B && fieldY <= hitBoxY2B) {
					printf("Answer B selected. ");
					checkAns('B', qID.ans);					// ANTWORT �BERPR�FEN
				}
				// ANTWORT C
				else if (fieldX >= hitBoxX1C && fieldX <= hitBoxX2C && fieldY >= hitBoxY1C && fieldY <= hitBoxY2B) {
					printf("Answer C selected. ");
					checkAns('C', qID.ans);					// ANTWORT �BERPR�FEN
				}
				// ANTWORT D
				else if (fieldX >= hitBoxX1D && fieldX <= hitBoxX2D && fieldY >= hitBoxY1D && fieldY <= hitBoxY2B) {
					printf("Answer D selected. ");
					checkAns('D', qID.ans);					// ANTWORT �BERPR�FEN
				}
				// CLICK IN JOKER-ZEILEN:
				else if (fieldY <= jPosY + hitBoxJArea && fieldY >= jPosY - hitBoxJArea) {
					// 50/50 JOKER
					if (fieldX >= jFifty.xPos - hitBoxJArea && fieldX <= jFifty.xPos + hitBoxJArea && jFifty.used == 0) {					// entsprechende Felder geklickt und Joker ist noch verf�gbar
						//printf("50/50 joker selected.\t used: %d\n", jFifty.used);
						removeJ(jFifty);
						jFifty.used = 1;
						// printf("50/50 joker used.\t used: %d\n\n", jFifty.used);
						useJ(jFifty);
					}
					// TELEFON-JOKER
					else if (fieldX >= jTel.xPos - hitBoxJArea && fieldX <= jTel.xPos + hitBoxJArea && jTel.used == 0) {					// entsprechende Felder geklickt und Joker ist noch verf�gbar
						removeJ(jTel);
						jTel.used = 1;
						useJ(jTel);
						redraw = 1;
					}
					// PUBLIKUMS-JOKER
					else if (fieldX >= jAud.xPos - hitBoxJArea && fieldX <= jAud.xPos + hitBoxJArea && jAud.used == 0) {					// entsprechende Felder geklickt und Joker ist noch verf�gbar
						removeJ(jAud);
						jAud.used = 1;
						useJ(jAud);
					}
					// ZUSATZ-JOKER
					else if (fieldX >= jSpecial.xPos - hitBoxJArea && fieldX <= jSpecial.xPos + hitBoxJArea && jSpecial.used == 0) {		// entsprechende Felder geklickt und Joker ist noch verf�gbar
						removeJ(jSpecial);
						jSpecial.used = 1;
						useJ(jSpecial);
					};
				};
			};
		}
		else Sleep(10);
	} while (gameOver != 1);							// VERARBEITE MAUSKLICKS SOLANGE KEINE FALSCHE ANTWORT GEGEBEN WURDE
}

// FUNKTION removeAns
// bekommt ein ansButton �bergeben
// graut den entsprechenden Button aus
void removeAns(ansButton button) {
	farbe2(button.x, button.y, LIGHTGRAY);
	resetAns = 1;
}

// FUNKTION histogramm
// bekommt 4 int-Werte
// zeigt die �bergebenen Prozentwerte als Histogramm an
void histogram(int a, int b, int c, int d) {
	groesse(1, 1);
	drawgui();
	printQ(qID);
	int answers[4] = { a, b, c, d };
	char ansLabels[4] = { 'A', 'B', 'C', 'D' };
	char percentageLabel[6];
	for (int xi = barPosX, i = 0; i < 4; i++, xi += 2 * sm) {
		switch (i) {
		case 0: {
			zeichen2(xi, 0, 'A');
			sprintf_s(percentageLabel, "%d %%", a);
			text2(xi, 3, percentageLabel);
			break;
		}
		case 1: {
			zeichen2(xi, 0, 'B');
			sprintf_s(percentageLabel, "%d %%", b);
			text2(xi, 3, percentageLabel);
			break;
		}
		case 2: {
			zeichen2(xi, 0, 'C');
			sprintf_s(percentageLabel, "%d %%", c);
			text2(xi, 3, percentageLabel);
			break;
		}
		case 3: {
			zeichen2(xi, 0, 'D');
			sprintf_s(percentageLabel, "%d %%", d);
			text2(xi, 3, percentageLabel);
			break;
		}
		}
		int max = (int)((double)answers[i] / 2 + 0.5);
		for (yi = 5; yi < max + 5; yi++) {
			form2(xi, yi, "s");
			farbe2(xi, yi, DARKBLUE);
		}
	}
	redraw = 1;
}
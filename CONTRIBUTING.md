# Team 23

#### A. Philip Hirt  
#### B. Philipp Warnecke

## Aufteilung:

- **(A) Grafische Oberfläche und Interaktion mit BOS**

- **(B) Funktion _randQ(int level)_**  
(zufällige Auswahl einer Frage der jeweiligen Gewinnstufe, Aufruf von _printQ(Question question)_)

- **(A) Funktion _checkAns(char choice, char correctAns)_**  
(überprüft die gewählte Antwort auf Richtigkeit)

- **(B) Funktion _checkJoker(Joker joker)_**  
(überprüft, ob der Joker verfügbar ist)

- **(A) Funktion _useJoker(Joker joker)_**  
(ruft die jeweilige Joker-Funktion auf)

- **(A,B) Joker**

- **(B) Fragen-Datenbank**